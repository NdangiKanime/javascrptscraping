const axios = require('axios'); // Library for making HTTP requests
const cheerio = require('cheerio'); // Library for parsing HTML

const url = 'https://books.toscrape.com/catalogue/category/books_1/index.html'; // Starting URL

// Function to scrape books data from a single page
async function scrapeBooksFromPage(pageUrl) {
  try {
    const response = await axios.get(pageUrl); // Fetch the HTML content
    const $ = cheerio.load(response.data); // Parse the HTML

    const books = [];
    $('.product_pod').each((index, element) => {
      const title = $(element).find('h3 a').text().trim();
      const price = $(element).find('.price_color').text().trim();

      books.push({ title, price });
    });

    return books;
  } catch (error) {
    console.error('Error scraping page:', error);
    return [];
  }
}

// Function to scrape books data from all pages (requires handling pagination)
async function scrapeAllBooks() {
  let allBooks = [];
  let currentPage = url;

  // Loop until there are no more pages (replace logic with actual pagination check)
  while (currentPage) {
    const booksFromPage = await scrapeBooksFromPage(currentPage);
    allBooks = allBooks.concat(booksFromPage);

    // Replace this with logic to find the next page URL based on HTML structure
    currentPage = null; // Assuming no pagination for simplicity

    // Example logic for following "Next" link (replace with actual selectors)
    // const nextPageLink = $('a.next').attr('href');
    // if (nextPageLink) {
    //   currentPage = 'https://books.toscrape.com' + nextPageLink;
    // }
  }

  return allBooks;
}

// Run the scraper and log the results
scrapeAllBooks()
  .then(books => console.log('Scraped Books:', books))
  .catch(error => console.error('Error scraping books:', error));
